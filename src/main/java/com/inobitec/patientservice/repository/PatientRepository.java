package com.inobitec.patientservice.repository;

import com.inobitec.patientservice.mapper.PatientMapper;
import com.inobitec.patientservice.model.Patient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;

@Repository
public class PatientRepository {

    @Autowired
    private PatientMapper patientMapper;

    public Patient selectPatientById(Integer id) {
        return patientMapper.selectPatientById(id);
    }

    public Patient selectPatientByFioAndNumber(String patientName, LocalDate birthday) {
        return patientMapper.selectPatientByFioAndNumber(patientName, birthday);
    }

    public void insertPatient(Patient patient) {
        patientMapper.insertPatient(patient);
    }

    public void updatePatient(Integer id, Patient patient) {
        patientMapper.updatePatient(id, patient);
    }

    public void deletePatient(Integer id) {
        patientMapper.deletePatient(id);
    }
}
