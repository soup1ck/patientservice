package com.inobitec.patientservice.mapper;

import com.inobitec.patientservice.model.Patient;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.time.LocalDate;

@Mapper
public interface PatientMapper {

    Patient selectPatientById(@Param("id") Integer id);
    Patient selectPatientByFioAndNumber(@Param("patientName") String patientName, @Param("birthday") LocalDate birthday);
    void insertPatient(@Param("p") Patient patient);
    void updatePatient(@Param("id") Integer id, @Param("p") Patient patient);
    void deletePatient(@Param("id") Integer id);
}
