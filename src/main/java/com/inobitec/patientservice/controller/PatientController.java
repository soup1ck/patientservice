package com.inobitec.patientservice.controller;

import com.inobitec.patientservice.model.Patient;
import com.inobitec.patientservice.repository.PatientRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;

@RestController
@RequestMapping("/patient")
@RequiredArgsConstructor
public class PatientController {

    private final PatientRepository patientRepository;

    @GetMapping("/{id}")
    public Patient getPatientById(@PathVariable Integer id) {
        return patientRepository.selectPatientById(id);
    }

    @GetMapping("/fio/birthday")
    public Patient getPatientByFioAndNumber(@RequestParam String patientName, @RequestParam String birthday) {
        return patientRepository.selectPatientByFioAndNumber(patientName, LocalDate.parse(birthday));
    }

    @PostMapping()
    public void createPatient(@RequestBody Patient patient) {
        patientRepository.insertPatient(patient);
    }

    @PutMapping("/{id}")
    public void updatePatient(@PathVariable Integer id, @RequestBody Patient patient) {
        patientRepository.updatePatient(id, patient);
    }

    @DeleteMapping("/{id}")
    public void deletePatient(@PathVariable Integer id) {
        patientRepository.deletePatient(id);
    }
}
